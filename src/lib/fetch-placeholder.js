// The following function is shared
// with getStaticProps and API routes
// from a `lib/` directory
export async function loadPlaceholderTodos() {
    // Call an external API endpoint to get posts
    const res = await fetch('https://jsonplaceholder.typicode.com/todos')
    // const res = await fetch('https://backend-mc4romn2kq-uc.a.run.app/api/catalog/departments/?expand=MEDIA')

    const data = await res.json()
  
    return data
}

export async function loadPlaceholderTodoById(params) {
  console.log('p', params)
  // Call an external API endpoint to get posts
  const res = await fetch(`https://jsonplaceholder.typicode.com/todos/${params}`)
  const data = await res.json()

  return data
}