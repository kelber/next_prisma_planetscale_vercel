import { createTodo } from '../../../lib/db'

export default async function handler(req,  res) {
  if(req.method === 'POST') {
    const data = JSON.parse(req.body)
    await createTodo(data)
    return res.status(200).json({ message: 'Sucesso' })
  }
}