import { Container } from "@mui/material"
import { getAllTodos } from "../../../lib/db"
import HideOnScroll from "../../components/HideOnScroll"
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useState } from "react";

// table
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';




export const getServerSideProps = async () => {
    const todos = await getAllTodos()
    return {
        props: {
            todos
        }
    }


}

// import { getAllTodos, Todo } from "../../../lib/db"
// interface Todo {
//     todos: Todo[];
// }

const Todos = ({ todos }) => {

    const [description, setDescription] = useState('')
    const handleClick = async () => {
        await fetch("/api/todo", {
            method: "POST",
            body: JSON.stringify(description)
        })
        description = ''
    }

    function createData(name) {
        return { name };
    }

    const rows = [
        createData('Frozen yoghurt', 159),

    ];

    return (
        <>
            <HideOnScroll />
            <Container maxWidth="fluid">

                <Box
                    sx={{
                        maxWidth: '100%',
                        margin: 3
                    }}
                >
                    <TextField fullWidth label="description" id="description"
                        value={description}
                        onChange={(e) => setDescription(e.currentTarget.value)}
                    />
                    <Button variant="contained" color="primary" mt="3"
                        onClick={() => handleClick()}
                    >Cadastrar

                    </Button>
                </Box>


                <h1>TODOS</h1>

                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 350 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">id</TableCell>
                                <TableCell align="left">Description</TableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {todos?.map((row) => (
                                <TableRow
                                    key={row.description}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell align="left">{row.description}</TableCell>
                                   
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>

                {/* <p>{JSON.stringify(todos, null, 4)}</p> */}

            
            </Container>

        </>
    )
}

export default Todos