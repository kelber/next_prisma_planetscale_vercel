import { Container } from "@mui/material"
import { useRouter } from "next/router"
import HideOnScroll from "../../components/HideOnScroll"
import { loadPlaceholderTodoById } from "../../lib/fetch-placeholder"


export async function getServerSideProps({params}) {
    console.log('params.id', params.id)

    // faça funcionar o loadPlaceholderTodoById()
    const res = await fetch(`https://jsonplaceholder.typicode.com/todos/${params.id}`)
    // const res = await loadPlaceholderTodoById(params.id)
    const data = await res.json()
    return { props: { data } }
  }


const Post = ({data}) => {
    
    return (
        <>
            <HideOnScroll />
            <Container maxWidth="fluid">

            <h1>Post  n. { data.id }</h1>
            <p>{ data.title }</p>

            </Container>
        </>
    )
}


export default Post