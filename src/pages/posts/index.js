
import HideOnScroll from '../../components/HideOnScroll'
import Container from '@mui/material/Container';
import { loadPlaceholderTodos } from '../../lib/fetch-placeholder';
import Link from 'next/link';
import { Button } from '@mui/material';



export async function getStaticProps() {
    const posts = await loadPlaceholderTodos()
    return { props: { posts } }
}


const Posts = ({ posts }) => {
    
    // const {count } = posts;
    // const [ posts ] = posts
    // let {count, posts: dp} = posts;
    // console.log(count);//"Sarah"
    // console.log(dp);//["Annie", "Becky"]
    // const [ posts ] = posts


    return (
        <div>
            <HideOnScroll />
            <Container maxWidth="fluid">
                <h1>posts</h1>

                   <ul>
                     {posts.map((post) =>

                        <Link key={post.id} href={`/posts/${post.id}`}  passHref> 
                            <li >{post.id} - {post.title}</li>
                        </Link>


                    )} 
                </ul>    


            </Container>
        </div>
    )
}

export default Posts
