

#### PACOTE MOSTER DE DESENVOLVIMENTO

NextJS + TAILWIND + PRISMA + PLANETSCALE COM MYSQL + VERCEL


*ao final resumo só do Prisma*



> npm i -D prisma

> npm i @prisma/client

> npx prisma init ==> configuração Global


schema.prisma.js
```js
generator client {
  provider = "prisma-client-js"
  previewFeatures = ["referentialIntegrity"]
}

datasource db {
  provider = "mysql"
  url      = env("DATABASE_URL")
  shadowDatabaseUrl = env("SHADOW_DATABASE_URL")
  referentialIntegrity = "prisma"
}


model todo  {
  id            Int @id @default(autoincrement())
  description   String @db.Text()

}


// Star que é o exemplo da Vercel
// https://www.youtube.com/watch?v=5JpKZfPx-1k


model Star {
  id            Int         @default(autoincrement()) @id
  createdAt     DateTime    @default(now())
  updatedAt     DateTime    @updatedAt
  name          String      @db.VarChar(255)
  consellation  String      @db.VarChar(255)
}



```

## PlanetScale

> pscale db create todo-app


.env
```js

DATABASE_URL=mysql://root@127.0.0.1:3309/nome-do-app 
SHADOW_DATABASE_URL=mysql://root@127.0.0.1:3310/nome-do-app 

// shadow é outra porta 

```

#### pscale auth --> ABRE browser confirmando logged in


*observação o TERMINAL usado é o BASH    //wsl.localhost....*

> pscale branch create todo-app nome-branch caso foi initial-setup

outra com nome  *shadow*



> pscale connect todo-app initial-setup --port 3309

// outro terminal

> pscale connect todo-app shadow --port 3310

// outro

> pscale branch promote todo-app main   <===================  
obs: criou um escudo na frente do main na pagina
qdo pulicar na vercel a main é a principal



#### migrando schemas
> $ npx prisma migrate dev --name init



#### Preparando arquivos

lib/prisma.ts
```js

// simples

import { PrismaClient } from '@prisma/client'
export const prisma = new PrismaClient();


// completo
import { PrismaClient } from '@prisma/client'

const prisma = global.prisma || new PrismaClient();

if(process.env.NODE_ENV === "development") global.prisma = prisma;

export default prisma



```



api/stars.js
```js
import prisma from "../../lib/prisma"



export default async function handler(req, res) {

    const { method } = req;

    switch (method) {
        case "GET":
            try {
                const stars = await prisma.star.findMany()
                res.status(200).json(stars)
            } catch (error) {
                console.error("Request error", error)
                res.status(500).json({ error: "Error fetching data" , error })    
            }
            break;
    
        default:
            res.setHeader("Allow", ["GET"])
            res.status(405).end(`Method ${method} is not allowed`)
            break;
    }




  }
  
```


> npx prisma studio para alimentar Stars  adicione  polaris > Ursa Minor

> npm run dev  e vá pra  api/stars    ---- OK ;)

vai no planetscale e console e veja se refletiu

######  PlanetScale

> select * from Star;   enter   ==>  reflete no planetScale  --> uhuu


overview ==> Btn create deploy Request ==> reflete para MAIN branch  ==> deploy changes e vai pra MAIN  :))) UHUUUU




### Voltando pro Notes


lib/db.ts
```js
import { prisma } from "./prisma";

export interface Todo {
    id: number;
    description: string;
}

export async function getAllTodos() {
    const data = await prisma.todo.findMany();
    return data;
}

export async function createTodo(description: string) {
    await prisma.todo.create({
        data: {
            description
        }
    })
}

```

api/todo.ts 
```js
// antigo arquivo hello :)

// criar vem nesse arquivo e getAllTodos vai no getStaticProps do respectivo arquivo

import type { NextApiRequest, NextApiResponse } from 'next'
import { createTodo } from '../../lib/db'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if(req.method === 'POST') {
    const data = JSON.parse(req.body)
    await createTodo(data)
    return res.status(200).json({ message: 'Sucesso' })
  }
}


// ou ===> veja o diretorio onde vc botou o lib  ==> Jesse é JS
import { createTodo } from '../../../lib/db'

export default async function handler(req,  res) {
  if(req.method === 'POST') {
    const data = JSON.parse(req.body)
    await createTodo(data)
    return res.status(200).json({ message: 'Sucesso' })
  }
}



```


pages/todos.js
```js
export const getServerSideProps = async () => {
    const todos = await getAllTodos()
    return {
        props: {
            todos
        }
    }
}

const Todos = () => {
    return (
        <>         
                <h1>Todos</h1>
                 <p>{JSON.stringify(todos, null, 4)}</p> 
        </>
    )
}

export default Todos
```



> npx prisma db push

o processo agora é de criar no site o "create deploy request" onde atualiza a main


todos
```js
import { Container } from "@mui/material"
import { getAllTodos } from "../../../lib/db"
import HideOnScroll from "../../components/HideOnScroll"
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { useState } from "react";

// table
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';


export const getServerSideProps = async () => {
    const todos = await getAllTodos()
    return {
        props: {
            todos
        }
    }


}

// exemplo interface Typescript
// import { getAllTodos, Todo } from "../../../lib/db"
// interface Todo {
//     todos: Todo[];
// }

const Todos = ({ todos }) => {

    const [description, setDescription] = useState('')
    const handleClick = async () => {
        await fetch("/api/todo", {
            method: "POST",
            body: JSON.stringify(description)
        })
        description = ''
    }

    function createData(name) {
        return { name };
    }

    // tabela
    const rows = [
        createData('Frozen yoghurt', 159),
    ];


    return (
        <>
            <HideOnScroll />
            <Container maxWidth="fluid">

                <Box
                    sx={{
                        maxWidth: '100%',
                        margin: 3
                    }}
                >
                    <TextField fullWidth label="description" id="description"
                        value={description}
                        onChange={(e) => setDescription(e.currentTarget.value)}
                    />
                    <Button variant="contained" color="primary" mt="3"
                        onClick={() => handleClick()}
                    >Cadastrar

                    </Button>
                </Box>


                <h1>Todos</h1>

                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 350 }} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">id</TableCell>
                                <TableCell align="left">Description</TableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {todos?.map((row) => (
                                <TableRow
                                    key={row.description}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell component="th" scope="row">
                                        {row.id}
                                    </TableCell>
                                    <TableCell align="left">{row.description}</TableCell>
                                   
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>

                {/* <p>{JSON.stringify(todos, null, 4)}</p> */}

            
            </Container>

        </>
    )
}

export default Todos



```


#### VERCEL


vá na planetscale => CONNECT COM select PRISMA

connect with pega url do DATABASE_URL

```js
DATABASE_URL='mysql://9nsdqe2zj2lm:************@pjctmf59zouf.us-east-3.psdb.cloud/todo-app?sslaccept=strict'
```


##### adicionar no package.json

"postinstall": "prisma generate"



# Prisma Resumo


> npm i -D prisma

> npm i @prisma/client

> npx prisma init ==> configuração Global


# FAÇA

1 - crie o model

2 - > pscale db create todo-app

3 - arrume o .env com DATABASE_URL

4 - pscale branch create todo-app nome-branch caso foi initial-setup E shadow

5 - pscale connect todo-app initial-setup --port 3309 e outra pscale connect todo-app shadow --port 3310

6 - pscale branch promote todo-app main  -->  cria um escudo na main


7 - npx prisma migrate dev --name init   --> migrando schemas

8 - lib/prisma.js  --> crie 


9 - ???  crie lib/db  e tb ??  api/todo  < == mesma coisa ? 

10 - npx prisma studio

11 - npx prisma db push






